import 'dart:convert';
import 'package:color/color.dart';
import 'package:crypto/crypto.dart';

class Member {
  /// The unique identifier of the member
  final String id;

  /// The name to display for the member
  final String? displayName;

  /// The color associated with the member
  late Color color;

  /// The reference to the avatar associated with the member
  final String? avatarReference;

  /// The type of the [Source]
  final String sourceType;

  /// The unique identifier of the [Source]
  final String sourceId;

  /// The color of the [Source]
  final Color sourceColor;

  @override
  int get hashCode => Object.hash(id, sourceType, sourceId);

  @override
  bool operator ==(Object other) {
    return other is Member &&
        id == other.id &&
        sourceType == other.sourceType &&
        sourceId == other.sourceId;
  }

  // Constructor
  Member(this.id, this.displayName, this.sourceType, this.sourceId,
      this.sourceColor,
      {Color? memberColor, this.avatarReference}) {
    if (memberColor != null) {
      color = memberColor;
    } else {
      color = Color.hex("#" +
          md5
              .convert(utf8.encode([id, sourceType, sourceId].join()))
              .toString()
              .substring(0, 5));
      color = Color.hsl(color.toHslColor().h, 90, 70);
    }
  }

  String getFormattedDisplayName() {
    return displayName ?? id;
  }
}
