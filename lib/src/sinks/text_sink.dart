import '../message.dart';
import '../member.dart';
import '../sink.dart';

abstract class TextSink implements Sink {
  /// Identifier of the sink
  @override
  final String type = "text";

  static String render(List<Message> messages, Set<Member> members) {
    // Init variables
    String output = "";

    // Iterate over messages
    for (var message in messages) {
      // Find the member that wrote the message
      var member = members.singleWhere((member) =>
          member.id == message.memberId &&
          member.sourceType == message.sourceType &&
          member.sourceId == message.sourceId);

      // Append the message to the output
      output += "${member.getFormattedDisplayName()}: ${message.text}\n";
    }

    return output;
  }
}
