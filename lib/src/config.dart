import 'dart:io';
import 'package:toml/toml.dart';
import 'utils.dart';
import 'constants.dart' as constants;

/// Get the path to the config location
String getPath() {
  // Get the default config location
  var configPath = constants.defaultConfigPath;

  // Find the home location based on the platform
  var home = findSystemHome();

  // Replace the home location if present
  if (home != null) {
    configPath = configPath.replaceAll("~", home);
  }

  return configPath;
}

/// Get the config
TomlDocument getConfig(String? configFileLocation) {
  // Get the config file location
  configFileLocation ??= "${getPath()}${constants.defaultConfigFilename}";

  // Check if the config exists
  var configFile = File(configFileLocation);
  if (!configFile.existsSync()) {
    configFile.writeAsStringSync(constants.defaultConfigFileContent);
  }

  // Load and parse the config file
  var configContent = configFile.readAsStringSync();
  return TomlDocument.parse(configContent);
}

/// Get the config
void writeConfig(Map config) {
  // Generate the toml config document
  var configContent = constants.defaultConfigFileContent;

  // Write the config file
  var configFile = File("${getPath()}${constants.defaultConfigFilename}");
  configFile.writeAsStringSync(configContent);
}
