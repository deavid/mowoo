import 'package:color/color.dart';
import 'package:mowoo/mowoo.dart';

import 'message.dart';
import 'member.dart';

abstract class MessagesUtility {
  static List<Map<String, dynamic>> toPlainList(
      List<Message> messages, Set<Member> members) {
    // The variable to be outputted
    List<Map<String, dynamic>> plainList = [];

    // Iterate over messages
    for (var message in messages) {
      // Find the member that wrote the message
      Member member;
      try {
        member = members.singleWhere((member) =>
            member.id == message.memberId &&
            member.sourceType == message.sourceType &&
            member.sourceId == message.sourceId);
      } catch (e) {
        member =
            Member("0", "unknown", "unknown", "unknown", Color.hex('#ffffff'));
      }

      // Append the message to the template messages
      plainList.add({
        "id": message.hashCode,
        "text": message.text,
        "timestamp": {
          "millisecondsSinceEpoch": message.timestamp.millisecondsSinceEpoch,
          "year": message.timestamp.year,
          "month": message.timestamp.month,
          "day": message.timestamp.day,
          "hour": message.timestamp.hour,
          "minute": message.timestamp.minute,
          "second": message.timestamp.second,
        },
        "member": {
          "displayName": member.displayName,
          "color": member.color.toHexColor().toCssString()
        },
        "source": {
          "type": message.sourceType,
          "id": message.sourceId,
          "color": message.sourceColor.toHexColor().toCssString(),
          "isOwncast": message.sourceType == "owncast",
          "isMatrix": message.sourceType == "matrix",
          "isIRC": message.sourceType == "irc",
          "isTwitch": message.sourceType == "twitch",
        },
      });
    }

    return plainList;
  }
}
