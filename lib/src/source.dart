import 'package:color/color.dart';
import 'middleware.dart';
import 'message.dart';
import 'member.dart';

class Source {
  /// Name of the souce
  final String name = "";

  /// Type of the source
  final String type = "";

  /// Color of the source
  final Color color = Color.hex("");

  /// Unique identifier for each source
  late final String id;

  /// List of messages
  final messages = <Message>{};

  /// List of members
  final members = <Member>{};

  /// List of requested middlewares
  late final List<String> requestedMiddlewares;

  /// List of available middlewares
  late final Map<String, Middleware> availableMiddlewares;

  // Constructor
  Source();

  @override
  int get hashCode => Object.hash(id, type);

  @override
  bool operator ==(Object other) {
    return other is Source && id == other.id && type == other.type;
  }

  /// Initialize the source
  ///
  /// Useful if the source has some preparation to do that requires async.
  Future<void> init() async {}

  /// Fetch messages from before the current time
  Future<void> fetchHistoricalMessages() async {}

  /// Fetch messages since a token
  Future<void> fetchNewMessages() async {}

  /// Update the list of current members
  Future<void> updateMembers() async {}

  /// Fetches the avatar for a [Member]
  ///
  /// Returns a Map with two keys:
  /// - "method" which equals one of the [avatarUtility.fetchMethods]
  /// - "data" containing the image binary data or an URL pointing to it
  Map<String, dynamic> transformAvatarId(Member member) {
    return {};
  }
}
