import 'dart:convert';
import 'package:mowoo/src/message.dart';
import 'package:mowoo/src/member.dart';
import 'package:mowoo/src/middleware.dart';

class HelloWorldMiddleware implements Middleware {
  @override
  final String name = "Hello World";
  @override
  final String type = "helloworld";
  late Map<String, dynamic> config;

  // Constructor
  HelloWorldMiddleware(this.config);

  @override
  Future<void> init() async {}

  @override
  Future<Map> process(Message? message, Member? member) async {
    if (message == null) {
      return {
        "message": message,
        "member": member,
      };
    }

    var words = message.text.split(" ");

    for (var i = 0; i < words.length; i++) {
      if (i.isEven) {
        words[i] = "hello";
      } else {
        words[i] = "world";
      }
    }

    message.text = words.join(" ");

    return {
      "message": message,
      "member": member,
    };
  }
}
