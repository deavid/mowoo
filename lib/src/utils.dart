import 'dart:io';

String? findSystemHome() {
  // Return the path to the home directory if present on the platform
  return Platform.environment['HOME'] ?? Platform.environment['USERPROFILE'];
}
