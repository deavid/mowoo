import 'dart:convert' show HtmlEscape;
import 'package:color/color.dart';
import 'package:matrix_api_lite/matrix_api_lite.dart' as matrix;
import '../middleware.dart';
import '../middleware_utility.dart';
import '../source.dart';
import '../member.dart';
import '../message.dart';

class MatrixSource implements Source {
  // Common variables
  @override
  final String name = "Matrix";
  @override
  final String type = "matrix";
  @override
  final Color color = Color.hex("#11ae85");
  @override
  late final String id;
  @override
  final messages = <Message>{};
  @override
  final members = <Member>{};
  @override
  late final List<String> requestedMiddlewares;
  @override
  late final Map<String, Middleware> availableMiddlewares;

  // Source specific variables
  late final String homeServer;
  late final String accessToken;
  late final String roomId;
  late final matrix.MatrixApi client;
  late String nextBatch;

  // Constructor
  MatrixSource(Map config, {Map<String, Middleware>? middlewares}) {
    // Set variables
    homeServer = config["home_server"];
    accessToken = config["access_token"];
    roomId = config["roomId"];
    id = homeServer;
    requestedMiddlewares = List<String>.from(config["middlewares"] ?? []);
    availableMiddlewares = middlewares ?? {};

    // Initialize the matrixApi client
    client = matrix.MatrixApi(
        homeserver: Uri(host: homeServer, scheme: "https"),
        accessToken: accessToken);
  }

  @override
  int get hashCode => Object.hash(id, type);

  @override
  bool operator ==(Object other) {
    return other is MatrixSource && id == other.id && type == other.type;
  }

  @override
  Future<void> init() async {
    // Perform initial sync with server
    final roomSync = await client.sync();
    nextBatch = roomSync.nextBatch;
  }

  @override
  Future<void> fetchHistoricalMessages() async {
    // Fetch events using API
    final roomEvents =
        await client.getRoomEvents(roomId, nextBatch, matrix.Direction.b);
    var sanitizer = const HtmlEscape();

    // For each event
    var chunk = roomEvents.chunk;
    if (chunk != null) {
      for (var event in chunk) {
        if (event.type == "m.room.message" &&
            event.content["msgtype"] == "m.text") {
          // Make new variables
          var newMessage = Message(
              event.eventId,
              sanitizer.convert(event.content["body"]),
              event.senderId,
              event.originServerTs,
              type,
              id,
              color);

          // Apply middlewares
          for (var middlewareName in requestedMiddlewares) {
            var middlewareFn = availableMiddlewares[middlewareName];
            if (middlewareFn != null) {
              var middlewareResult =
                  await middlewareFn.process(newMessage, null);
              newMessage = middlewareResult["message"];
            }
          }

          // Add message to messages
          messages.add(newMessage);
        }
      }
    }
  }

  @override
  Future<void> fetchNewMessages() async {
    // Fetch events using API
    final roomEvents =
        await client.getRoomEvents(roomId, nextBatch, matrix.Direction.f);
    var sanitizer = const HtmlEscape();

    // For each event
    var chunk = roomEvents.chunk;
    if (chunk != null) {
      for (var event in chunk) {
        if (event.type == "m.room.message" &&
            event.content["msgtype"] == "m.text") {
          // Make new variables
          var newMessage = Message(
              event.eventId,
              sanitizer.convert(event.content["body"]),
              event.senderId,
              event.originServerTs,
              type,
              id,
              color);

          // Apply middlewares
          var middlewareResult = await MiddlewareUtility.apply(
              newMessage, null, requestedMiddlewares, availableMiddlewares);
          newMessage = middlewareResult["message"];

          // Add message to messages
          messages.add(newMessage);
        }
      }
    }

    nextBatch = roomEvents.end!;
  }

  @override
  Future<void> updateMembers() async {
    // Fetch the members of the room
    var membersFetch = await client.getMembersByRoom(roomId,
        membership: matrix.Membership.join);

    // For each member of the room
    for (var m in membersFetch!) {
      members.add(Member(
        m.senderId,
        m.content["displayname"],
        type,
        id,
        color,
        avatarReference: m.content["avatar_url"],
      ));
    }
  }

  @override
  Map<String, dynamic> transformAvatarId(Member member) {
    // var avatarMXC = m.content["avatar_url"];
    // var avatarMatches = avatarRe.firstMatch(avatarMXC);

    // var avatarRe = RegExp(r"mxc\:\/\/(.*)\/(.*)");

    // if (avatarMatches != null) {
    //   var serverName = avatarMatches.group(1)!;
    //   var mediaId = avatarMatches.group(2)!;

    //   var avatarFile = await client.getContent(serverName, mediaId);
    //   print(avatarFile.contentType);
    //   if (avatarFile.contentType == "image/jpeg") {
    //     Image image = decodeJpg(avatarFile.data)!;
    //     File('cool-awesome-avatar-yo.png').writeAsBytesSync(encodePng(image));
    //   }
    // }

    // TODO: implement getAvatarUrlForMember
    throw UnimplementedError();
  }
}
